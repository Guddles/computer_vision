import cv2
import numpy as np


def colors(figures):
    colors ={"first" :0, "second" :0, "third": 0, "fourth":0, "fifth": 0, "sixth":0}
    #hue - выдуманные оттенки
    for hue in figures:
        if (0 <= hue < 20 or 170 <= hue <= 190):
            colors['first'] += 1
        elif (20 <= hue < 50):
            colors['second'] += 1
        elif (50 <= hue < 80):
            colors['third'] += 1
        elif (80 <= hue < 120):
            colors['fourth'] += 1
        elif (120 <= hue < 150):
            colors['fifth'] += 1
        elif (150 <= hue < 190):
            colors['sixth'] += 1

    return colors


image = cv2.imread("balls_and_rects.png")
cv2.namedWindow("Image",cv2.WINDOW_GUI_NORMAL)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
_, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)



rects = []
circles =[]

for i, contour in enumerate(contours):
    if i == 0:
        continue
    eps = 0.01*cv2.arcLength(contour, True)
    approx = cv2.approxPolyDP(contour, eps, True)
    
    cv2.drawContours(image, contour, -1, (0,255,0), 4)
    
    x,y,w,h = cv2.boundingRect(approx)
    

    
    if (len(approx) == 4):
        rects.append(round(np.mean(approx)/10))

    else:
        
        circles.append(round(np.mean(approx)/10))
        
        

print("прямоугольников ", len(rects))
print("кругов ", len(circles))


print("rects color", colors(rects))
print("circle color", colors(circles))

cv2.imshow("Image", image)
cv2.waitKey(0)
cv2.destroyAllWindows()