import cv2
import numpy as np
import math
import time

cv2.namedWindow("Camera", cv2.WINDOW_FULLSCREEN)

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_FPS, int(30))
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, 10)

bal_l = np.array([0, 70, 200])
bal_u = np.array([120, 210, 255])

coords = [0, 0]
speed = 0 
m_time = round(time.time()) * 100

while cam.isOpened():
    ret, frame = cam.read()
    # _, thresh = cv2.threshold(frame, 0, 120,0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask_o = cv2.inRange(hsv, bal_l, bal_u)
    mask_o = cv2.erode(mask_o, None, iterations=2)
    mask_o = cv2.dilate(mask_o, None, iterations=2)
    cnt = cv2.findContours(mask_o, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    # cv2.drawContours(frame, cnt, -1, (0,255,0),2)
    if len(cnt) > 0:
        c = max(cnt, key=cv2.contourArea)
        (x,y), radius = cv2.minEnclosingCircle(c)
        if radius > 20:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0,255,0),2)
            n_x = x - coords[0]
            n_y = y  - coords[1]
            # print(radius)
            pix=288
            distance = ((pow(n_x, 2)+pow(n_y,2))**0.5)/pix
            n_time = time.time()
            speed = distance / (n_time - m_time)
            m_time = round(time.time())
            coords = [0, 0]
            cv2.putText(frame, f"Speed: {abs(round(speed/100, 1))} m/s", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0))
            
    
    
    key = cv2.waitKey(1)
    if key == ord('d'):
        break
    cv2.imshow("Camera", frame)
cam.release()
cv2.destroyAllWindows()