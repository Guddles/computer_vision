import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as ndimage
from skimage.measure import label, regionprops


mask = np.array([
                  np.array([
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 0, 0, 1, 1],
                            [1, 1, 0, 0, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 0, 0, 1, 1],
                            [1, 1, 0, 0, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 1, 1],
                            [1, 1, 1, 1],
                            [1, 1, 0, 0],
                            [1, 1, 0, 0],
                            [1, 1, 1, 1],
                            [1, 1, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 1, 1],
                            [1, 1, 1, 1],
                            [0, 0, 1, 1],
                            [0, 0, 1, 1],
                            [1, 1, 1, 1],
                            [1, 1, 1, 1]
                  ])
], dtype=object)


img = np.load("ps.npy")
arr = []
reg_count = label(ndimage.binary_erosion(img,mask[0]))
arr.append(reg_count.max())

for i in range(1, len(mask)):
    other_fig = label(ndimage.binary_erosion(img, mask[i]))
    if i in [3,4]:
        arr.append(other_fig.max() - arr[0])
    else:
        arr.append(other_fig.max())
print("total figures ", np.array(arr).sum(), "\n", "regct ", arr[0], "\n",\
      "open up ", arr[1], "\n", "open down ", arr[2],"\n", "open right ", arr[3],"\n",\
          "open left ", arr[4])
# labeled = label(img)
# plt.imshow(labeled)
plt.show()
