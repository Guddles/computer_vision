from pathlib import Path
import time
from skimage.filters import gaussian
from skimage.io import imread
from time import perf_counter
import matplotlib.pyplot as plt
from threading import Thread
from queue import Queue
import cv2
import glob

def to_bin(path):
    img = cv2.imread(str(path))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    gb = cv2.GaussianBlur(gray, (215,235), 0)
    ret, thresh = cv2.threshold(gb, 140, 190, 0)
    
    
    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    cv2.drawContours(img, contours, -1, (0,255,0),0)
    # cv2.imshow("Pen", img)
    # print(thresh)
    # print(contours)
    return thresh, contours, img

def find_points(path):
    thresh, contours, img = to_bin(path)
    area_arr = []
    for i, countour in enumerate(contours):
        if i ==0:
            continue
        # print(cv2.arcLength(countour, True))
        M = cv2.moments(countour)
        x = int(M["m10"] / M["m00"])
        y = int(M["m01"] / M["m00"])
        area = cv2.contourArea(countour, True)
        cv2.putText(img, str(area), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, 255)
        # print(area)
        area_arr.append(area)
        
        
    # cv2.imshow("Pen", img)
    return area_arr
pencils_count = list()
count = 0

def is_pencil(path):
    global count
    global pencils_count
    for i in find_points(path):
        # print(i)
        if ((i <570000) and (i>340000)):
            # print("true")
            count+=1
    pencils_count.append(count)
    
    
t = perf_counter()
for i in range(1, 13):
    path = Path(f"C:/HomeWork/pencils/images/img ({i}).jpg")
    is_pencil(path)
print("Количество корандашей на всех картинках ",pencils_count[-1])
print(f"Time elapsed {perf_counter() -t}")
# def func(path):
#     # image = imread(path).mean(2)
#     # gauss = gaussian(image, 200)
#     # return image / gauss
#     return is_pencil(path)

# def process_image(q_in, q_out):
#     while True:
#         path = q_in.get()
#         if isinstance(path, int):
#             break
#         q_out.put(path, func(path))


# path = "C:/HomeWork/pencils/images/*.jpg"
# path = [Path(f"C:/HomeWork/pencils/images/img ({i}).jpg") for i in range(1,13)]

# files = list(path.glob("*.jpg"))


# threads = []
# tasks = Queue()
# result = Queue()
# for i in range(4):
#     threads.append(Thread(target=process_image, args=(tasks, result)))
#     threads[-1].start()
    

# for _ in range(4):
#     tasks.put(-1)

# for file in files:
#     tasks.put(file)

# for th in threads:
#     th.join()
# print(result.qsize())

# plt.imshow(result)
# plt.show()

# cv2.namedWindow("Pen", cv2.WINDOW_KEEPRATIO)
# cv2.imshow("Pencil", )
# to_bin(path)
# find_points(path)

# cv2.waitKey()
# cv2.destroyAllWindows()