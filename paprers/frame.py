import cv2

img = cv2.imread("frame.jpg")




converted = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(converted, 120, 255, 0)
contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# cv2.drawContours(img, contours, 22, (0,255,0),2)

# print(len(contours))
area = cv2.contourArea(contours[22], True)
cv2.putText(img, str(area), (30,30), cv2.FONT_HERSHEY_SIMPLEX, 1, 255)

# for i, contour in enumerate(contours):
#     if i == 0 :
#         continue
#     M = cv2.moments(contour)
#     x = int(M["m10"] / M["m00"])
#     y = int(M["m01"] / M["m00"])
    
#     area = cv2.contourArea(contour, True)
#     cv2.putText(img, str(area), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, 255)
#     print(area)
# contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# print(cv2.contourArea(contours))
cv2.namedWindow("Frame", cv2.WINDOW_KEEPRATIO)
cv2.imshow("Frame",img)
cv2.waitKey(0)
cv2.destroyAllWindows()
    
