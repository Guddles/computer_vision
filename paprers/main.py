import cv2
import numpy as np


cap = cv2.VideoCapture("video.mp4")

cv2.namedWindow("Frames")


while cap.isOpened():
    ret, frame = cap.read()
    converted = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    ret, thresh = cv2.threshold(converted, 120, 255, 0)

    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    
    for i, contour in enumerate(contours):
        if i ==0:
            continue
        area = cv2.contourArea(contour, True)
        if area == 33785.5 or area == 6153.0:
            cv2.putText(frame, str("IT'S MINE YEAH"), (30,30), cv2.FONT_HERSHEY_SIMPLEX, 1, 255)

    
    cv2.imshow("Frames", frame)
    key = cv2.waitKey(1)
    if key == ord('d'):
        break
    if key == ord('s'):
        cv2.imwrite("frame.jpg", frame)
    if key == ord('q'):
        print(contours)

cap.release()

cv2.destroyAllWindows()